//
//  CarBuilder.h
//  BuilderPatternCarSample
//
//  Created by Vladyslav Bedro on 10/16/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import <Foundation/Foundation.h>

//classes
#import "Car.h"

NS_ASSUME_NONNULL_BEGIN

@interface CarBuilder : NSObject

//properties
@property (strong, nonatomic) Car* car;

//methdos
- (Car*) getCar;

//abstract methdos
- (void) setModel;
- (void) setEngine;
- (void) setAutoPilotVersion;
- (void) setWheelsSize;
- (void) setSpeed;

@end

NS_ASSUME_NONNULL_END
