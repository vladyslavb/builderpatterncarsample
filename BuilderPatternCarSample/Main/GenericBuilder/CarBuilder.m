//
//  CarBuilder.m
//  BuilderPatternCarSample
//
//  Created by Vladyslav Bedro on 10/16/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "CarBuilder.h"

@implementation CarBuilder


#pragma mark - Initialization -

- (id) init
{
    if (self = [super init])
    {
        self.car = [[Car alloc] init];
    }
    
    return self;
}


#pragma mark - Public methods -

- (Car*) getCar
{
    return self.car;
}


#pragma mark - Public abstact methods -

- (void) setModel            {}

- (void) setEngine           {}

- (void) setAutoPilotVersion {}

- (void) setWheelsSize       {}

- (void) setSpeed            {}

@end
