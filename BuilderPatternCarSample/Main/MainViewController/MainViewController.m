//
//  MainViewController.m
//  BuilderPatternCarSample
//
//  Created by Vladyslav Bedro on 10/16/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "MainViewController.h"

//clases
#import "SlowSpeedSimpleCarBuilder.h"
#import "HighSpeedSportCarBuilder.h"

#import "FactoryCarsParking.h"

@interface MainViewController ()

//outlets
@property (weak, nonatomic) IBOutlet UIImageView*        carModelImageView;
@property (weak, nonatomic) IBOutlet UISegmentedControl* carModelControl;

//properties
@property (strong, nonatomic) SlowSpeedSimpleCarBuilder* slowCarBuilder;
@property (strong, nonatomic) HighSpeedSportCarBuilder*  highCarBuilder;

@property (strong, nonatomic) FactoryCarsParking* carsParking;

@end

@implementation MainViewController


#pragma mark - Life cycle -

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    [self configureCarBuilders];
}


#pragma mark - Memory managment -

- (void) didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark - Internal methods -

- (void) configureCarBuilders
{
    self.slowCarBuilder = [[SlowSpeedSimpleCarBuilder alloc] init];
    self.highCarBuilder = [[HighSpeedSportCarBuilder  alloc] init];
    
    self.carsParking = [[FactoryCarsParking alloc] init];
}


#pragma mark - Actions -

- (IBAction) carModelControlPressed: (UISegmentedControl*) sender
{
    if(sender.selectedSegmentIndex == 0)
    {
        [self.carsParking setBuilder: self.slowCarBuilder];
    }
    else
    {
        [self.carsParking setBuilder: self.highCarBuilder];
    }
    
    [self.carsParking constructCar];
    
    Car* car = [self.carsParking getCar];
    
    self.carModelImageView.image = [UIImage imageNamed: car.model];

    NSLog(@"Car Model = %@, \n"
          "engine = %@,     \n"
          "autoPilot = %@,  \n"
          "wheelSize = %ld, \n"
          "speed = %ld",
          car.model,
          car.engine,
          car.autoPilotVersion,
          car.wheelsSize,
          car.speed);
}


@end
