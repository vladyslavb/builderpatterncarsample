//
//  FactoryCarsParking.h
//  BuilderPatternCarSample
//
//  Created by Vladyslav Bedro on 10/16/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import <Foundation/Foundation.h>

//clases
#import "Car.h"
#import "CarBuilder.h"

NS_ASSUME_NONNULL_BEGIN

@interface FactoryCarsParking : NSObject

//properties
@property (nonatomic, strong) CarBuilder* carBuilder;

//methods
- (void) setBuilder: (CarBuilder*) carBuilder;

- (void) constructCar;

- (Car*) getCar;

@end

NS_ASSUME_NONNULL_END
