//
//  FactoryCarsParking.m
//  BuilderPatternCarSample
//
//  Created by Vladyslav Bedro on 10/16/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "FactoryCarsParking.h"

@implementation FactoryCarsParking

- (void) setBuilder: (CarBuilder*) aBuilder
{
    self.carBuilder = aBuilder;
}

- (void) constructCar
{
    [self.carBuilder setModel];
    [self.carBuilder setEngine];
    [self.carBuilder setAutoPilotVersion];
    [self.carBuilder setWheelsSize];
    [self.carBuilder setSpeed];
}

- (Car*) getCar
{
    return self.carBuilder.getCar;
}

@end
