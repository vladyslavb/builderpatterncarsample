//
//  HighSpeedSportCarBuilder.h
//  BuilderPatternCarSample
//
//  Created by Vladyslav Bedro on 10/16/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "CarBuilder.h"

NS_ASSUME_NONNULL_BEGIN

@interface HighSpeedSportCarBuilder : CarBuilder

@end

NS_ASSUME_NONNULL_END
