//
//  HighSpeedSportCarBuilder.m
//  BuilderPatternCarSample
//
//  Created by Vladyslav Bedro on 10/16/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "HighSpeedSportCarBuilder.h"

@implementation HighSpeedSportCarBuilder


#pragma mark - Overrided methods -

- (void) setModel
{
    self.car.model  = @"Tesla";
}

- (void) setEngine
{
    self.car.engine = @"VE8";
}

- (void) setAutoPilotVersion
{
    self.car.autoPilotVersion = @"5.0.1Ve";
}

- (void) setWheelsSize
{
    self.car.wheelsSize = 20;
}

- (void) setSpeed
{
    self.car.speed  = 400;
}

@end
