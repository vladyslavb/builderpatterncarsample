//
//  SlowSpeedSimpleCarBuilder.m
//  BuilderPatternCarSample
//
//  Created by Vladyslav Bedro on 10/16/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "SlowSpeedSimpleCarBuilder.h"

@implementation SlowSpeedSimpleCarBuilder


#pragma mark - Overrided methods -

- (void) setModel
{
    self.car.model  = @"VAZ";
}

- (void) setEngine
{
    self.car.engine = @"V1";
}

- (void) setAutoPilotVersion
{
    self.car.autoPilotVersion = @"WithoutAutoPilot";
}

- (void) setWheelsSize
{
    self.car.wheelsSize = 14;
}

- (void) setSpeed
{
    self.car.speed  = 100;
}

@end
